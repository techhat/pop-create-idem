ACCT Profiles
=============
`acct` profile information is implicitly passed to the appropriate function calls via the `ctx` parameter.

.. code-block:: python

    # __func_alias__ makes sure the "list_" function is put on the hub as "list"
    # We use this for function names that clash with python builtin names
    __func_alias__ = {"list_": "list"}


    async def list_(hub, ctx):
        await hub.tool.my_cloud.request(ctx, "api/reference/to/resource/list")

ACCT Plugins
============
ACCT
Simple and secure account management

USAGE
Yaml files containing confidential information can be encrypted for use inside of acct base applications.
This is an example of what an acct credentials file might look like.


# credentials.yml

.. code-block: yaml

        provider:
          profile_name:
            username: XXXXXXXXXXXX
            password: XXXXXXXXXXXX
            api_key: XXXXXXXXXXXXXXXXXXX

Next use the acct command to encrypt this file using the fernet algorithm:
$ acct encrypt credentials.yml
YeckEnWEGOjBDVxxytw13AsdLgquzhCtFHOs7kDsna8=
The acct command can also be used to decrypt the encrypted file:
$ acct decrypt credentials.yml.fernet --output=yaml --acct-key="YeckEnWEGOjBDVxxytw13AsdLgquzhCtFHOs7kDsna8="

INTEGRATION

CLI
Your own app can extend acct's command line interface to use the --acct-file and --acct-key options:

.. code-block:: python

    # my_project/conf.py
    CLI_CONFIG = {
        "acct_file": {"source": "acct", "os": "ACCT_FILE"},
        "acct_key": {"source": "acct", "os": "ACCT_KEY"},
        "crypto_plugin": {"source": "acct"},
    }

In your own project, you can vertically merge acct and extend it with your own plugins:

.. code-block:: python

    # my_project/conf.py
    DYNE = {
        "acct": ["acct"],
    }

PLUGINS
Create the directory  my_project/acct/my_project and add your acct plugins there.
acct plugins need to implement a gather function.
If your app used "hub.acct.init.unlock()" to initialize profile data then you can read profiles from
hub.acct.PROFILES as a starting point for sub_profile data.
gather() should return a dictionary of processed profiles.
Gather functions can be asynchronous or synchronous.



.. code-block:: python

    # my_project/acct/my_project/my_plugin.py
    async def gather(hub):
        """
        Get [my_plugin] profiles from an encrypted file

        Example:

        .. code-block:: yaml

            my_plugin:
              profile_name:
                username: XXXXXXXXXXXX
                password: XXXXXXXXXXXX
                api_key: XXXXXXXXXXXXXXXXXXXXXXX
        """
        processed_profiles = {}
        for profile, ctx in hub.acct.PROFILES.get("my_plugin", {}).items():
            # Create a connection through [some_library] for each of the profiles
            sub_profiles[profile] = {
                "connected": False,
                "connection": await some_library.connect(**ctx),
            }
        return processed_profiles

The gather function can optionally take a "profiles" parameter.
"profiles" will be an implicitly passed dictionary of data that was read from the encrypted acct file.
This is useful when profiles are collected explicitly by your own program and aren't stored in the
traditional location within acct.

.. code-block:: python

    # my_project/acct/my_project/my_plugin.py
    async def gather(hub, profiles):
        """
        Get [my_plugin] profiles from an encrypted file

        Example:

        .. code-block:: yaml

            my_plugin:
              profile_name:
                username: XXXXXXXXXXXX
                password: XXXXXXXXXXXX
                api_key: XXXXXXXXXXXXXXXXXXXXXXX
        """
        processed_profiles = {}
        for profile, ctx in profiles.get("my_plugin", {}).items():
            # Create a connection through [some_library] for each of the profiles
            sub_profiles[profile] = {
                "connected": False,
                "connection": await some_library.connect(**ctx),
            }
        return processed_profiles

acct plugins can also define how to shut down or close the connections made in a profile.
Simply include a function called "close" in your plugin that takes a "profiles" parameter.
These profiles will be the processed profiles from the gather function.

.. code-block:: python

    async def close(hub, profiles):
        """
        Define how to close the connections for the profile
        """
        for name, sub_profile in profiles.items():
            await sub_profile.connection.close()

BACKENDS
You can make an acct backend plugin to collect profiles from an alternate source.
Backends are processed after the initial encrypted acct_file reading, but before profiles are processed.
This makes it so you can define credentials to access an external secret store in your acct_file --
and then define extra profiles within that external secret store.  acct backend plugins transform data from
that external secret store into acct profiles.  unlock functions optionally take a profiles parameter
and can be synchronous or asynchronous.

.. code-block:: python

    # my_project/acct/my_project/my_plugin.py
    async def unlock(hub):
        """
        Get profiles from an external store

        Example:

        .. code-block:: yaml

            backend_key: my_backend_key

            my_backend_key:
              my_plugin:
                profile_name:
                  username: XXXXXXXXXXXX
                  password: XXXXXXXXXXXX
                  api_key: XXXXXXXXXXXXXXXXXXXXXXX
        """
        backends = hub.acct.PROFILES.get(hub.acct.BACKEND_KEY, {})
        profiles = {}
        for profile, ctx in backends.get("my_plugin", {}).items():
            # Create a connection through [some_library] for each of the profiles
            connection = await some_library.connect(**ctx)

            # get profile information from the connection and turn it into a dictionary that looks like:
            # {"provider": "profile1":  {"kwarg1": "value1"}
            profiles.update(connection.get_profiles())

        return profiles

INTERNAL
Add acct startup code to your project's initializer:

.. code-block:: python

    # my_project/my_project/init.py
    def __init__(hub):
        # Horizontally merge the acct dynamic namespace into your project
        hub.pop.sub.add(dyne_name="acct")


    def cli(hub):
        # Load the config from evbus onto hub.OPT
        hub.pop.config.load(["my_project", "acct"], cli="my_project")

        # decrypt the encrypted file using the given key and populate hub.acct.PROFILES with the decrypted structures
        hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)

        # Process profiles from subs in `hub.acct.my_project` and put them into `hub.acct.SUB_PROFILES`
        # return the explicitly named profile
        profile = hub.acct.init.gather(
            subs=["my_project"], profile=hub.OPT.acct.acct_profile
        )

Alternatively, your app can collect profiles explicitly without storing them on the hub:

.. code-block:: python

    # my_project/my_project/init.py
    def __init__(hub):
        # Horizontally merge the acct dynamic namespace into your project
        hub.pop.sub.add(dyne_name="acct")


    def cli(hub):
        hub.pop.loop.create()
        hub.pop.Loop.run_until_complete(_run(hub))


    async def _run(hub):
        # Load the config from evbus onto hub.OPT
        hub.pop.config.load(["my_project", "acct"], cli="my_project")

        # Collect profiles without storing them on the hub
        profiles = await hub.acct.init.profiles(
            hub.OPT.acct.acct_file, hub.OPT.acct.acct_key
        )

        # Collect subs without storing them on the hub
        sub_profiles = await hub.acct.init.process(["my_project"], profiles)

        # Retrieve a specifically named profile
        profile = await hub.acct.init.single(
            profile_name=hub.OPT.acct.acct_profile,
            subs=["my_project"],
            sub_profiles=sub_profiles,
            profiles=profiles,
        )
